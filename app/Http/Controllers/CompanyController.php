<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Company;

class CompanyController extends Controller
{
    public function index()
    {

        return view('company.index',[
        'companies' => Company::all()
    ]);

    }

    public function create()
    {
        return view('company.create');

    }

    public function store(Request $request)
    {
        $request->validate([
            'id' => 'integer',
            'nama' => 'required|string|max:255',
            'alamat' => 'required|string|max:255'
        ]);

        Company::create($request->all());
        return redirect()->route('company.index')->with('success', 'Company has been added!');
    }

    public function edit(Company $company)
    {
        return view('company.edit', compact('company'));

    }

    public function update(Request $request, Company $company)
    {
        $request->validate([
            'id' => 'integer',
            'nama' => 'required|string|max:255',
            'alamat' => 'required|string|max:255'
        ]);

        $company->update($request->all());

        return redirect()->route('company.index')->with('success', 'Company telah berhasil diupdate');


    }

    public function destroy(Company $company)
    {
        $company->delete();

        return redirect()->route('company.index')->with('Success', 'Comapny Berhasil di Hapus');
    }
}

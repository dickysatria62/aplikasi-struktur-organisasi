<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use Illuminate\Http\Request;
use PhpParser\Node\Expr\Empty_;

class EmployeeController extends Controller
{
    public function index()
    {

        return view('employee.index',[
        'employees' => Employee::all()
    ]);

    }

    public function index2()
    {
        return view('welcome');
    }

    public function create()
    {
        return view('employee.create');

    }

    public function store(Request $request)
    {
        $request->validate([
            'id' => 'integer',
            'nama' => 'required|string|max:255',
            'atasan_id' => 'integer',
            'company_id' => 'required|int'
        ]);

        Employee::create($request->all());
        return redirect()->route('employee.index')->with('success', ' Your Employee has been added!');
    }

    public function edit(Employee $employee)
    {
        return view('employee.edit', compact('employee'));

    }

    public function update(Request $request, Employee $employee)
    {
        $request->validate([
            'id' => 'required',
            'nama' => 'required',
            'atasan_id' => 'required',
            'company_id' => 'required'
        ]);

        $employee->update($request->all());

        return redirect()->route('employee.index')->with('success', 'Kategori telah berhasil diupdate');


    }

    public function destroy(Employee $employee)
    {
        $employee->delete();

        return redirect()->route('employee.index')->with('Success', 'Employee Berhasil di Hapus');
    }
}

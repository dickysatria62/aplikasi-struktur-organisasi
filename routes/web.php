<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\WelcomeController;
use App\Http\Controllers\EmployeeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', function () {
    return view('welcome', [
    ]);
});

Route::get('/employee', function () {
    return view('employee.index',[
    ]);
});
Route::get('/company', function () {
    return view('company.index',[
    ]);
});

Route::resource('/employee', App\http\Controllers\EmployeeController::class);;
Route::resource('/company', App\http\Controllers\CompanyController::class);;

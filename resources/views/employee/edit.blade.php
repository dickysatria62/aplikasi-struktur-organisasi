@extends('layouts.main')

@section('container')

<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1 class="h2">Edit Employee</h1>
  </div>

  <div class="col-lg-8">
  <form method="post" action="{{ route('employee.update', $employee->id) }}" class="mb-5"
    enctype="multipart/form-data">
    @csrf
    @method('put')
    <div class="mb-3">
        <label for="id" class="form-label">ID</label>
        <input type="number" class="form-control" id="id"
        name="id">
      </div>
    <div class="mb-3">
      <label for="nama" class="form-label">Nama</label>
      <input type="text" class="form-control" id="nama"
      name="nama">
    </div>
    <div class="mb-3">
      <label for="atasan_id" class="form-label">Atasan ID</label>
      <input type="number" class="form-control " id="atasan_id"
      name="atasan_id">
    </div>
    <div class="mb-3">
      <label for="company_id" class="form-label">Company ID</label>
      <input type="number" class="form-control" id="company_id"
      name="company_id">
    </div>
    <button type="submit" class="btn btn-primary">Update Employee</button>
  </form>
</div>
@endsection

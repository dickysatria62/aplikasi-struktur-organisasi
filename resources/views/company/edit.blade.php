@extends('layouts.main')

@section('container')

<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1 class="h2">Edit Company</h1>
  </div>

  <div class="col-lg-8">
  <form method="post" action="{{ route('company.update', $company->id) }}" class="mb-5"
    enctype="multipart/form-data">
    @csrf
    @method('put')
    <div class="mb-3">
        <label for="id" class="form-label">ID</label>
        <input type="number" class="form-control" id="id"
        name="id">
      </div>
    <div class="mb-3">
      <label for="nama" class="form-label">Nama</label>
      <input type="text" class="form-control" id="nama"
      name="nama">
    </div>
    <div class="mb-3">
      <label for="alamat" class="form-label">Alamat</label>
      <input type="text" class="form-control" id="alamat"
      name="alamat">
    </div>
    <button type="submit" class="btn btn-primary">Update Company</button>
  </form>
</div>
@endsection

<nav id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block bg-light sidebar collapse">
    <div class="position-sticky pt-3">
      <ul class="nav flex-column">
        <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1
        text-muted">
            <span>Dashboard Employee</span>
          </h6>

        <li class="nav-item">
          <a class="nav-link {{ Request::is('employee*') ? 'active' : ''}}" aria-current="page" href="employee">
            <span data-feather="file-text"></span>
            Employee
          </a>
        </li>

        <li class="nav-item">
          <a class="nav-link {{ Request::is('company*') ? 'active' : ''}}" href="company">
            <span data-feather="home"></span>
            Company
          </a>
        </li>
      </ul>
    </div>
  </nav>
